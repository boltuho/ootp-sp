/*
* Определите функцию MyPrintf(), получающую printf-подобную строку форматирования, которая может содержать директивы: 
* %c (символ), %s (строка), %d (целое типа int) или %f(вещественное типа double), и произвольное количество других
* аргументов. Не пользуйтесь функцией printf(). Используйте макросы определенные в <stdarg.h>.
* */
#include "string.h"
#include "sstream"
#include "iostream"
#include "stdarg.h"

using namespace std;

int MyPrintf(const char *format, ...) {
    va_list args;
    stringstream ss;
    va_start(args, format);
    for (int i = 0; i < strlen(format); i++) {
        if (format[i] == '%') {
            char arg = format[++i];
            if (arg == 'c') {
                char c = va_arg(args, char*)[0];
                ss << c;
            }
            else if (arg == 's') {
                char *str = va_arg(args, char*);
                ss << str;
            }
            else if (arg == 'd') {
                int num = va_arg(args, int);
                ss << num;
            }
            else if (arg == 'f') {
                double num = va_arg(args, double);
                ss << num;
            }
            else {
                cerr << "Wrong argument %" << arg << endl;
                return -1;
            }
        }
        else {
            ss << format[i];
        }
    }
    va_end(args);
    cout << ss.str();
    return 0;
}

int main() {
    MyPrintf("%d + %d = %d\n", 3, 3, 6);
    MyPrintf("%f + %f = %f\n", 2.2, 0.7, 2.9);
    MyPrintf("%c is the third letter in the alphabet.\n", "c");
    MyPrintf("Hello %s. How are %s?\n", "user", "you");
    return 0;
}
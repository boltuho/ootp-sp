#include <iostream>
#include "strstream"
#include "stdio.h"

using namespace std;

class TextFile {
private:
    const char *name;
    FILE *fDescriptor;
public:
    TextFile(const char *name) : name(name) {
        fDescriptor = fopen(name, "a+");
        if (fDescriptor == NULL) {
            throw "Can't open file.";
        }
    }

    TextFile &TextFile::operator=(const TextFile &file) {
        strstream fCopyName;
        fCopyName << name << "_copy";
        TextFile copyFile(fCopyName.str());
        char *text = getText(file);
        copyFile += text;
        return copyFile;
    }

    TextFile &TextFile::operator=(const char *text) {
        fDescriptor = fopen(name, "w");
        fprintf(fDescriptor, text);
        fDescriptor = fopen(name, "a+");
        return *this;
    }

    TextFile &TextFile::operator+=(const TextFile &file) {
        fprintf(fDescriptor, getText(file));
        return *this;
    }

    TextFile &TextFile::operator+=(const char *text) {
        fprintf(fDescriptor, text);
        return *this;
    }


    friend char *getText(const TextFile &);

    virtual ~TextFile() {
        fclose(fDescriptor);
    }

};


char *getText(TextFile const &file) {
    strstream buffer;
    FILE *f = file.fDescriptor;
    int c;
    while ((c = getc(f)) != EOF) {
        buffer << (char) c;
    }

    return buffer.str();
}

int main() {
    TextFile fishTextFile("lorem_file");
    TextFile logFile("log");

//    Добавление строк в файл
    logFile += "Hello How are you?";
    logFile += "Hi, I'm ok!";

//Добавление содержимого одного файла в другой
    logFile += fishTextFile;
    fishTextFile += logFile;

//    Удаление старого контента файла и запись нового
    logFile = "Changed log file content.";
    fishTextFile = "File was cleaned and rewrited.";

    return 0;
}
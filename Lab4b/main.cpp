/*
* Задание В1. Создайте класс «Оружие», который содержит следующую информацию: сила удара и сила защиты.
Метод «ударить» класса «Оружие» генерирует случайное число в диапазоне от 0 до «сила удара», уменьшает «сила удара»
на значение равное частному от деления сгенерированного числа на 10 и возвращает сгенерированное число.
Метод «защитить» класса «Оружие» получает силу удара оружия противника, генерирует случайное число в диапазоне от 0 до
«сила защиты», уменьшает «сила защиты» на значение равное частному от деления сгенерированного числа на 10, вычисляет
разницу между силой удара и сгенерированным числом и, если результат положительный то возвращается это число, если нет
то возвращается 0. Возвращаемое значение есть сила повреждения.
Создайте классы «Щит», «Меч», «Булава» производные от «Оружие» и переопределите в них функции «ударить» и «защитить» в
соответствии с видом оружия.
* */

#include <iostream>
#include <string.h>

using namespace std;

class Weapon {
private:
    int damage, protection;
public:
    const int DEFAULT_DAMAGE = 9;
    const int DEFAULT_PROTECTION = 8;

    Weapon() {
        protection = DEFAULT_PROTECTION;
        damage = DEFAULT_DAMAGE;
    }

    virtual unsigned int attack() {
        int generatedDamage = ((double) rand() / (double) RAND_MAX) * this->damage;
        this->damage -= generatedDamage / 10;

        return generatedDamage;
    }

    virtual unsigned int protect(int damage) {
        int generatedProtection = ((double) rand() / (double) RAND_MAX) * this->protection;
        this->protection -= generatedProtection / 10;
        int protectionValue = damage - generatedProtection;

        return protectionValue > 0 ? protectionValue : 0;
    }
};

class Sword : public Weapon {
public:
    virtual unsigned int attack() override {
        return Weapon::attack() * 1.5;
    }

    virtual unsigned int protect(int damage) override {
        return Weapon::protect(damage) * 0.95;
    }
};

class Shield : public Weapon {
public:
    virtual unsigned int attack() override {
        return Weapon::attack() * 0.4;
    }

    virtual unsigned int protect(int damage) override {
        return Weapon::protect(damage) * 0.2;
    }
};

class Baton : public Weapon {

public:
    virtual unsigned int attack() override {
        return Weapon::attack() * 3;
    }

    virtual unsigned int protect(int damage) override {
        return Weapon::protect(damage) * 1.5;
    }
};

class Knight {
private:
    int health;
    Weapon *weapon;
    char *name;
public:

    Knight(const char *name, unsigned int health, Weapon *weapon) : health(health), weapon(weapon) {
        this->name = new char[128];
        strcpy(this->name, name);
    }

    bool isAlive() {
        return health > 0;
    }

    Weapon *getWeapon() const {
        return weapon;
    }

    int attack(Knight *k) {
        return k->protect(this->getWeapon());
    }

    int protect(Weapon *weapon) {
        int damage = this->weapon->protect(weapon->attack());
        health -= damage;
        return damage;
    }

    virtual ~Knight() {
        delete name;
        delete weapon;
    }
};


int main() {
    Knight *arthur = new Knight("Arthur", 120, new Baton);
    Knight *persival = new Knight("Persival", 130, new Sword);

    int damage;
    while (arthur->isAlive() && persival->isAlive()) {
        damage = arthur->attack(persival);
        cout << "[Arthur] Attacked Persival and inflicted damage in " << damage << " points." << endl;
        damage = persival->attack(arthur);
        cout << "[Persival] Attacked Arthur and inflicted damage in " << damage << " points." << endl;
    }

    if (arthur->isAlive()) {
        cout << "Arthur won!" << endl;
    }
    else {
        cout << "Persival won!" << endl;
    }

    delete arthur;
    delete persival;
    return 0;
}

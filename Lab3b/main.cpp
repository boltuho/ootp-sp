/*
* Составить описание класса, обеспечивающего представление матрицы произ­вольного размера с возможностью изменения
* числа строк и столбцов, вывода на экран подматрицы любого размера и всей матрицы.
* */
#include <iostream>

using namespace std;

const unsigned int DEFAULT_SIZE = 5;

class Matrix {
private:
    unsigned int n, m;
    int **matrix;

    int **initializeMatrix(int n, int m) {
        int **matrix = new int *[n];
        for (int i = 0; i < m; i++) {
            matrix[i] = new int[m];
        }
        return matrix;
    }

    void fillMatrix() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = (rand() % 9);
            }
        }
    }

    void freeMemory(int **matrix, int n) {
        for (int i = 0; i < n; i++) {
            delete matrix[i];
        }
    }

public:
    Matrix(unsigned int n, unsigned int m) : n(n), m(m) {
        matrix = initializeMatrix(n, m);
        fillMatrix();
    }

    Matrix() : n(DEFAULT_SIZE), m(DEFAULT_SIZE) {
        matrix = initializeMatrix(DEFAULT_SIZE, DEFAULT_SIZE);
        fillMatrix();
    }

    void changeSize(unsigned int newNSize, unsigned int newMSize) {
        int **newMatrix = initializeMatrix(newNSize, newMSize);
        for (int i = 0; i < newNSize; i++) {
            for (int j = 0; j < newMSize; j++) {
                if (i < n && j < m) {
                    newMatrix[i][j] = matrix[i][j];
                }
                else {
                    newMatrix[i][j] = 0;
                }
            }
        }
        freeMemory(matrix, n);
        matrix = newMatrix;
        n = newNSize;
        m = newMSize;
    }

    void showSubmatrix(int n, int m) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cout << matrix[i][j] << "   ";
            }
            cout << endl;
        }
    }

    void show() {
        showSubmatrix(n, m);
    }

    virtual ~Matrix() {
        freeMemory(matrix, n);
    }
};

class MatrixManagementMenu {
private:
    bool closed;
    Matrix matrix;

public:
    MatrixManagementMenu(Matrix const &matrix) : matrix(matrix) {
    }

private:
    void performAction(int actionNumber) {
        switch (actionNumber) {
            case 1:
                matrix.show();
                break;
            case 2:
                showSubmatrix();
                break;
            case 3:
                changeMatrixSize();
                break;
            case 4:
                closed = true;
                break;
            default:
                cout << "Sorry, that's is not correct action...";
        }
    }

    int readActionNumber() {
        int actionNumber;
        cin >> actionNumber;
        return actionNumber;
    }

    void showSubmatrix() {
        unsigned int n, m;
        cout << "Enter submatrix size N: ";
        cin >> n;
        cout << "Enter submatrix size M: ";
        cin >> m;
        matrix.showSubmatrix(n, m);
    }

    void changeMatrixSize() {
        unsigned int n, m;
        cout << "Enter new matrix size N: ";
        cin >> n;
        cout << "Enter new matrix size M: ";
        cin >> m;
        matrix.changeSize(n, m);
    }

public:
    bool isClosed() {
        return closed;
    }

    void show() {
        cout << "Welcome to matrix management menu. Choose neccessary action:" << endl;
        cout << "1. Show matrix content" << endl;
        cout << "2. Show submatrix" << endl;
        cout << "3. Change matrix size" << endl;
        cout << "4. Exit" << endl;
        cout << "Enter action number: ";
        int actionNumber = readActionNumber();
        performAction(actionNumber);
    }
};

int main() {
    Matrix defaultSizeMatrix;
    MatrixManagementMenu managementMenu(defaultSizeMatrix);
    while (!managementMenu.isClosed()) {
        managementMenu.show();

    }
    return 0;
}
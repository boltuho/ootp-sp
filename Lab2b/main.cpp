/*
* Составить описание класса треугольников координатами концов в трехмерном пространстве. В конструкторе обеспечить проверку,
* является ли сумма углов равна 180. Определить является ли треугольник тупоугольным, прямоугольным или остроугольным.
* */

#include <iostream>
#include <math.h>

using namespace std;

class Point {
public:

    Point(int x, int y, int z) : x(x), y(y), z(z) {
    }

    int getX() const {
        return x;
    }

    int getY() const {
        return y;
    }

    int getZ() const {
        return z;
    }

private:
    double x, y, z;
};

class Vector {
private:
    double x, y, z;

public:
    double getSize() {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

    double getAngle(Vector *v) {
        double vectorMultiple = x * v->x + y * v->y + z * v->z;

        return acos(vectorMultiple / (getSize() * v->getSize())) * 180.0 / M_PI;
    }

    Vector(Point const &a, Point const &b) {
        x = abs(b.getX() - a.getX());
        y = abs(b.getY() - a.getY());
        z = abs(b.getZ() - a.getZ());
    }
};

class Triangle {
private:
    Vector *a, *b, *c;

    Vector *getBigSide(Vector *v1, Vector *v2, Vector *v3) {
        Vector *bigSide = v1;
        if (v2->getSize() > bigSide->getSize()) {
            bigSide = v2;
        }
        if (v3->getSize() > bigSide->getSize()) {
            bigSide = v3;
        }
        return bigSide;
    };

public:
    Triangle(Point apex1, Point apex2, Point apex3) {
        a = new Vector(apex1, apex2);
        b = new Vector(apex2, apex3);
        c = new Vector(apex3, apex1);

        double mountOfAngles = b->getAngle(a) + c->getAngle(b) + a->getAngle(c);
        cout << "Mount: " << mountOfAngles << endl;

        if (mountOfAngles != 180) {
            throw "Sum of the angles should be 180 degrees";
        }
    }

    char *getType() {
        Vector *bigSide = getBigSide(a, b, c);
        Vector *c1, *c2;
        if (a == bigSide) {
            c1 = b;
            c2 = c;
        }
        else if (b == bigSide) {
            c1 = a;
            c2 = c;
        }
        else {
            c1 = a;
            c2 = b;
        }


        if (pow(bigSide->getSize(), 2) == pow(c1->getSize(), 2) + pow(c2->getSize(), 2)) {
            return (char *) "rectangular";
        }
        else if (pow(bigSide->getSize(), 2) < (pow(c2->getSize(), 2) + pow(c2->getSize(), 2))) {
            return (char *) "acute-angled";
        }
        else {
            return (char *) "obtuse";
        }
    }
};

static const int ARRAY_SIZE = 10;

int main() {
    try {
        Triangle *acuteTriangle = new Triangle(Point(1, 1, 2), Point(4, 1, 0), Point(4, 5, 0));
        cout << "Type of triangle: " << acuteTriangle->getType() << endl;

        Triangle *rectangularTriangle = new Triangle(Point(1, 1, 0), Point(4, 1, 0), Point(4, 5, 0));
        cout << "Type of triangle: " << rectangularTriangle->getType() << endl;
    }
    catch (const char *e) {
        cerr << e << endl;
        return -1;
    }

    return 0;
}
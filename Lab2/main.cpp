/*
* Состав класса Music (музыкальное произведение): название произведения, количество проданных копий,
* возможность скачать mp3 в интернете. Подсчитать общую количество проданных копий и количество произведении,
* которые можно скачать mp3 в интернете.
* */

#include <iostream>
#include <string.h>
#include <bits/string3.h>

using namespace std;

class Music {
private:
    const char *name;
    int soldCopies;
    bool free;
public:

    Music() {
    }

    Music(const char *name, int soldCopies, bool free) : name(name), soldCopies(soldCopies), free(free) {
    }


    char const *getName() const {
        return name;
    }

    int getSoldCopies() const {
        return soldCopies;
    }


    bool isFree() const {
        return free;
    }

    virtual ~Music() {
        delete name;
    }
};

class MusicUtils {
public:
    static unsigned int countSoldCopies(Music *m, int n) {
        unsigned int result = 0;
        for (int i = 0; i < n; i++) {
            result += m[i].getSoldCopies();
        }
        return result;
    }

    static unsigned int countFreeTracks(Music *m, int n) {
        unsigned int result = 0;
        for (int i = 0; i < n; i++) {
            if (m[i].isFree()) {
                result++;
            }
        }
        return result;
    }
};


static const int ARRAY_SIZE = 10;

static const int NAME_SIZE = 128;

Music createTrack(char const *name, unsigned int soldCopies, bool isFree);

int main() {
    Music *trackList = new Music[ARRAY_SIZE];

    trackList[0] = createTrack("Korn  – Kiss", 501, true);
    trackList[1] = createTrack("Korn  – Right now", 5403, false);
    trackList[2] = createTrack("Korn  – Liar", 1024, false);
    trackList[3] = createTrack("Korn  – Evolution", 512, true);
    trackList[4] = createTrack("Korn  – One", 928, false);
    trackList[5] = createTrack("Korn  – Clown", 54, true);
    trackList[6] = createTrack("Korn  – Hold on", 5114, true);
    trackList[7] = createTrack("Korn  – Get up", 1204, false);
    trackList[8] = createTrack("Korn  – Twist", 256, false);
    trackList[9] = createTrack("Korn  – Hater", 514, true);

    cout << "Count of sold copies: " << MusicUtils::countSoldCopies(trackList, ARRAY_SIZE) << endl;
    cout << "Count of free tracks available for download: " << MusicUtils::countFreeTracks(trackList, ARRAY_SIZE) << endl;

    return 0;
}

Music createTrack(char const *name, unsigned int soldCopies, bool isFree) {
    char *dynName = new char[NAME_SIZE];
    strcpy(dynName, name);
    return Music(dynName, soldCopies, isFree);
}


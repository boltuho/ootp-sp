//Имеется элемент стека (дисциплина обслуживания LIFO) и стек
#include <iostream>

using namespace std;

template<class T>
struct StackItem {
    T *data;
    StackItem<T> *prev;
};

template<class T>
class Stack {
private:
    StackItem<T> *stack;
public:
    Stack() {
        stack = NULL;
    }

    bool isEmpty() {
        return stack == NULL;
    }

    T &pop() {
        if (isEmpty()) {
            throw "Sorry. Stack is empty";
        }
        else {
            StackItem<T> *popItem = stack;
            stack = stack->prev;
            return *popItem->data;
        }
    }

    void push(const T data) {
        struct StackItem<T> *newItem = new StackItem<T>;
        newItem->prev = NULL;
        newItem->data = new T;
        *newItem->data = data;
        if (isEmpty()) {
            stack = newItem;
        }
        else {
            newItem->prev = stack;
            stack = newItem;
        }
    }

    virtual ~Stack() {
        if (!isEmpty()) {
            StackItem<T> *temp = stack;
            while (temp != NULL) {
                delete stack;
                temp = temp->prev;
                stack = temp;
            }
        }
    }
};

template<class T>
class StackManagementMenu {
private:
    bool closed;
    Stack<T> *stack;

    void popItem() {
        try {
            cout << "Pop number from stack: " << stack->pop() << endl;
        }
        catch (const char *e) {
            cerr << e << endl;
        }
    }

    void performAction(int actionNumber) {
        switch (actionNumber) {
            case 1:
                pushNewItem();
                break;
            case 2:
                popItem();
                break;
            case 3:
                cout << "Stack is empty: " << stack->isEmpty() << endl;
                break;
            case 4:
                closed = true;
                break;
            default:
                cout << "Sorry, that's is not correct action...";
        }
    }

    int readActionNumber() {
        int actionNumber;
        cin >> actionNumber;
        return actionNumber;
    }

    void pushNewItem() {
        int number;
        cout << "Enter new stack item: ";
        cin >> number;
        stack->push(number);
    }

public:
    StackManagementMenu(Stack<T> *stack) : stack(stack) {
        closed = false;
    }

    bool isClosed() {
        return closed;
    }

    void show() {
        cout<<endl;
        cout << "Welcome to stack management menu. Choose neccessary action:" << endl;
        cout << "1. Push new item" << endl;
        cout << "2. Pop item" << endl;
        cout << "3. Check is stack empty" << endl;
        cout << "4. Exit" << endl;
        cout << "Enter action number: ";
        int actionNumber = readActionNumber();
        performAction(actionNumber);
    }

};

int main() {
    Stack<int> *stack = new Stack<int>;
    StackManagementMenu<int> managementMenu(stack);

    while (!managementMenu.isClosed()) {
        managementMenu.show();

    }
    delete stack;
    return 0;
}
/*
* Задание А3. Класс описывает мебель и содержит следующие данные-члены: производитель, цена и материал.
Создайте класс «Шкаф» производный от «Мебель» содержит дополнительную информацию: ширину, глубину и высоту.
Создайте класс «Стол» производный от «Мебель» содержит дополнительную информацию: количество ящиков, площадь рабочей поверхности.
Создайте класс «Стул» производный от «Мебель» содержит дополнительную информацию: возможность регулировать положение спинки.
* */

#include <iostream>
#include <string.h>
#include <vector>
#include <strstream>

using namespace std;

class Furniture {
private:
    char *producer;
    unsigned int cost;
    char *material;

    void allocateMemory() {
        producer = new char[256];
        material = new char[64];
    }

public:
    Furniture() {
        allocateMemory();
    }

    Furniture(const char *producer, const char *material, unsigned int cost) : cost(cost) {
        allocateMemory();
        setProducer(producer);
        setMaterial(material);
    }


    Furniture(char *producer, unsigned int cost) : cost(cost) {
        allocateMemory();
        setProducer(producer);
        setMaterial("<unknown>");
    }


    Furniture(unsigned int cost, const char *material) : cost(cost) {
        allocateMemory();
        setProducer("<unknown>");
        setMaterial(material);
    }

    char *getProducer() const {
        return producer;
    }

    void setProducer(const char *producer) {
        strcpy(this->producer, producer);
    }

    unsigned int getCost() const {
        return cost;
    }

    void setCost(unsigned int cost) {
        Furniture::cost = cost;
    }

    char *getMaterial() const {
        return material;
    }

    void setMaterial(const char *material) {
        strcpy(this->material, material);
    }

    virtual char *toString() {
        strstream ss;
        ss << "Producer:" << getProducer() << ", ";
        ss << "Material:" << getMaterial() << ", ";
        ss << "Cost:" << getCost();
        return ss.str();
    }

    virtual ~Furniture() {
        delete producer;
        delete material;
    }
};

class Cabinet : public Furniture {
private:
    unsigned int height, width, depth;
public:
    Cabinet(char const *producer, char const *material, unsigned int cost) : Furniture(producer, material, cost) {
    }

    Cabinet() : Furniture() {
    }

    unsigned int getHeight() const {
        return height;
    }

    void setHeight(unsigned int height) {
        Cabinet::height = height;
    }

    unsigned int getWidth() const {
        return width;
    }

    void setWidth(unsigned int width) {
        Cabinet::width = width;
    }

    unsigned int getDepth() const {
        return depth;
    }

    void setDepth(unsigned int depth) {
        Cabinet::depth = depth;
    }

    virtual char *toString() {
        strstream ss;
        ss << "Futniture:Cabinet, ";
        ss << Furniture::toString();
        ss << ", ";
        ss << "Height:" << getHeight() << ", ";
        ss << "Width:" << getWidth() << ", ";
        ss << "Depth:" << getDepth();

        return ss.str();
    }
};

class Table : public Furniture {
private:
    unsigned int numberOfBoxes, workingSurfaceSquare;

public:

    Table() : Furniture() {
    }

    Table(char const *producer, char const *material, unsigned int cost) : Furniture(producer, material, cost) {
    }


    Table(char const *producer, char const *material, unsigned int cost, unsigned int numberOfBoxes, unsigned int workingSurfaceSquare)
            : Furniture(producer, material, cost),
              numberOfBoxes(numberOfBoxes),
              workingSurfaceSquare(workingSurfaceSquare) {
    }

    unsigned int getNumberOfBoxes() const {
        return numberOfBoxes;
    }

    void setNumberOfBoxes(unsigned int numberOfBoxes) {
        Table::numberOfBoxes = numberOfBoxes;
    }

    unsigned int getWorkingSurfaceSquare() const {
        return workingSurfaceSquare;
    }

    void setWorkingSurfaceSquare(unsigned int workingSurfaceSquare) {
        Table::workingSurfaceSquare = workingSurfaceSquare;
    }

    virtual char *toString() {
        strstream ss;
        ss << "Futniture:Table, ";
        ss << Furniture::toString();
        ss << ", ";
        ss << "NumberOfBoxes:" << getNumberOfBoxes() << ", ";
        ss << "WorkingSureaceSquare:" << getWorkingSurfaceSquare();
        return ss.str();
    }
};

class Chair : public Furniture {
private:
    bool adjustable;

public:

    Chair() : Furniture() {
    }

    Chair(char const *producer, char const *material, unsigned int cost) : Furniture(producer, material, cost) {
    }

    bool isAdjustable() const {
        return adjustable;
    }

    void setAdjustable(bool adjustable) {
        Chair::adjustable = adjustable;
    }

    virtual char *toString() {
        strstream ss;
        ss << "Futniture:Chair, ";
        ss << Furniture::toString();
        ss << ", ";
        ss << "Adjustable:" << isAdjustable();
        return ss.str();
    }
};

class FurnitureManagementMenu {
private:
    bool closed;
    vector<Furniture *> furniture;

    void performAction(int actionNumber) {
        switch (actionNumber) {
            case 1: {
                for (std::vector<Furniture *>::iterator it = furniture.begin(); it != furniture.end(); ++it) {
                    Furniture *f = *it;
                    cout << f->toString() << endl;
                }
                cout << endl;
                break;
            }
            case 2: {
                Cabinet *cabinet = new Cabinet;
                enterFurnitureData(cabinet);
                enterCabinetData(cabinet);
                furniture.push_back(cabinet);
                break;
            }
            case 3: {
                Table *table = new Table;
                enterFurnitureData(table);
                enterTableData(table);
                furniture.push_back(table);
                break;
            }
            case 4: {
                Chair *chair = new Chair;
                enterFurnitureData(chair);
                enterChairData(chair);
                furniture.push_back(chair);
                break;
            }
            case 5:
                closed = true;
                break;
            default:
                cout << "Sorry, that's is not correct action...";
        }
    }

    int readActionNumber() {
        int actionNumber;
        cin >> actionNumber;
        return actionNumber;
    }

public:

    bool isClosed() {
        return closed;
    }

    void show() {
        cout << "Welcome to the furniture storage. Choose neccessary action:" << endl;
        cout << "1. Show available furniture" << endl;
        cout << "2. Add cabinet" << endl;
        cout << "3. Add table" << endl;
        cout << "4. Add chair" << endl;
        cout << "5. Exit" << endl;
        cout << "Enter action number: ";
        int actionNumber = readActionNumber();
        performAction(actionNumber);
    }

    void enterFurnitureData(Furniture *f) {
        char producer[256], material[64];
        int cost;
        cout << "Enter producer name: ";
        cin >> producer;
        cout << "Enter material: ";
        cin >> material;
        cout << "Enter cost: ";
        cin >> cost;

        f->setProducer(producer);
        f->setMaterial(material);
        f->setCost(cost);
    }

    void enterCabinetData(Cabinet *c) {
        unsigned height, width, depth;
        cout << "Enter cabinet height: ";
        cin >> height;
        cout << "Enter cabinet width: ";
        cin >> width;
        cout << "Enter cabinet depth: ";
        cin >> depth;
        c->setHeight(height);
        c->setWidth(width);
        c->setDepth(depth);
    }

    void enterTableData(Table *t) {
        unsigned int numberOfBoxes, workingSurfaceSquare;
        cout << "Enter number of boxes: ";
        cin >> numberOfBoxes;
        cout << "Enter working surface size: ";
        cin >> workingSurfaceSquare;

        t->setNumberOfBoxes(numberOfBoxes);
        t->setWorkingSurfaceSquare(workingSurfaceSquare);
    }

    void enterChairData(Chair *c) {
        bool adjustable;
        cout << "Enter is chair adjustable: ";
        cin >> adjustable;

        c->setAdjustable(adjustable);
    }


    virtual ~FurnitureManagementMenu() {
        for (std::vector<Furniture *>::iterator it = furniture.begin(); it != furniture.end(); ++it) {
            Furniture *f = *it;
            delete f;
        }
    }
};


int main() {
    FurnitureManagementMenu menu;
    while (!menu.isClosed()) {
        menu.show();
    }
    return 0;
}